const express = require('express');
// const path = require('path');
const glob = require('glob');
const fs = require('fs-extra');
const sharp = require('sharp');
// const md5 = require('js-md5');

const app = express();
app.set('view engine', 'ejs');
// const SKIP_GROUPS = 0;

const MAX_GROUPS = 6000;
const MIN_GROUP_SIZE = 2;
const MAX_GROUP_SIZE = 999;
const MAX_TIME_DIFF = 1 * 60 * 60;
const BASE_PATH = '/Users/pkowalewski/Dropbox/Camera\ Uploads/';
const TRASH_PATH = '/Users/pkowalewski/Dropbox/Camera\ Uploads \Trash/';

const getAllImages = async () => {
    return new Promise(resolve => {
        glob(BASE_PATH + "**/*\.{jpg,jpeg,png}", {
            nocase: true
        }, function(er, files) {
        	console.log('all files: ' + files.length);
            resolve(files);
        });
    });
};

const getFilenameFromPath = (fullPath) => {
	const fileName = fullPath.split('/').pop();
	const ext = fileName.split('.').pop();
	// console.log(fileName.replace('.' + ext, ''));
	return fileName.replace('.' + ext, '');
};

const getFileStatSync = (filePath) => {
	const fileName = getFilenameFromPath(filePath);
	const d = new Date(fileName.slice(0,19).replace(/\./g, ':'));
	if (d instanceof Date && !isNaN(d)) {
		return d.getTime() / 1000;
	} else {
		console.log(fs.statSync(filePath).birthtime);
		return fs.statSync(filePath).birthtime;
	}

};

const mapImagesAndDates = (allImages) => {
    const result = [];
    allImages.map((file) => {
        result[file] = getFileStatSync(file);
    });
    return result;
};

const groupImagesByDates = (imagesWithDates) => {
    const groups = [
        []
    ];
    let lastIndex = 0;
    let lastTime = null;
    for (const [path, time] of Object.entries(imagesWithDates)) {
    	// console.log(time - lastTime);
        if (lastTime !== null && time - lastTime > MAX_TIME_DIFF) {
            // console.log(time);
            lastIndex++;
            groups[lastIndex] = [];
        }

        lastTime = time;

        groups[lastIndex].push({
        	time,
        	path
        });
    }

    const filteredGroups = groups.filter((singleGroup) => {
    	if (MAX_GROUP_SIZE !== null && singleGroup.length > MAX_GROUP_SIZE) {
    		return false;
    	}

    	return singleGroup.length >= MIN_GROUP_SIZE
    });
    
    const sortedGrups = filteredGroups.sort((a, b) => {
        if (a[0] < b[0]) {
            return -1;
        } else {
            return 1;
        }
    });

    return sortedGrups;
};

const index = async (req, res) => {
	const page = req.query.page || 1;

    const allImages = await getAllImages();
    const sortedAllImage = allImages.sort();
    const imagesMap = mapImagesAndDates(sortedAllImage.sort());
    const groups = groupImagesByDates(imagesMap);

	const slicedGroups = groups.slice(MAX_GROUPS * (page - 1), MAX_GROUPS * page);

    res.render('index', {
    	page : req.query.page || 1,
        groups : slicedGroups
    });
};

const thumb = (req, res) => {
    fs.access(req.query.file, (err) => {
        if (!err) {
        	try {
	            const readStream = fs.createReadStream(req.query.file);
	            let transform = sharp().rotate().resize(400);
	            readStream.pipe(transform).pipe(res);
	        } catch (err) {
	        	console.log(err);
	        	res.end();
	        }
        } else {
            res.writeHead('200', {
                'Content-Type': 'image/png'
            });
            return res.end(new Buffer('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==', 'base64'), 'binary');
        }
    });
};

const preview = (req, res) => {
    fs.access(req.query.file, (err) => {
        if (!err) {
            const readStream = fs.createReadStream(req.query.file);
            let transform = sharp().rotate().resize(1440);
            readStream.pipe(transform).pipe(res);
        } else {
            res.writeHead('200', {
                'Content-Type': 'image/png'
            });
            return res.end(new Buffer('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==', 'base64'), 'binary');
        }
    });
};

const trash = (req, res) => {
    const from = req.query.file;
    const to = from.replace(BASE_PATH, TRASH_PATH);
    fs.copy(from, to).then(() => {
        res.send('plik w koszu');
    }).catch(err => {
        console.error(err);
        res.send(err);
    });
};

app.get('/', index);
app.get('/thumb', thumb);
app.get('/preview', preview);
app.get('/trash', trash);
// app.get('/rotate', rotate);
app.listen(3000);
console.log('Running on http://localhost:3000');