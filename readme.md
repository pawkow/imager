# IMAGER

To nigdy nie miało być publiczne.

Aplikacja nie kasuje zdjęć. Przenosi je do folderu. 

## Konfiguracja (zalecana)

```
const MAX_GROUPS = 200;
const MIN_GROUP_SIZE = 5;
const MAX_GROUP_SIZE = null;
const MAX_TIME_DIFF = 1 * 60 * 60; // 60 minutes
const BASE_PATH = '/Users/__username__/obrazy/';
const TRASH_PATH = '/Users/__username__/obrazy-trash/';
```

## Podgląd

![Podgląd](doc/preview.png?raw=true "Podgląd")

## TODO
Zastosować offset zamiast `?page` (aby nie omijać grup przy przechodzeniu do kolejnej strony)
